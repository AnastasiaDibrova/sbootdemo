package com.spdu.springboot.sbd.topic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TopicJdbcTemplateDao implements TopicDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Topic> getTopics() {
        String sql = "SELECT * FROM Topic";

        return jdbcTemplate.query(sql,
                (RowMapper<Topic>) (rs, rowNum) -> {
            Topic topic = new Topic();

            topic.setId(rs.getInt("id"));
            topic.setName(rs.getString("title"));
            topic.setDescription(rs.getString("description"));

            return topic;
        });
    }

    @Override
    public Topic getTopic(int id) {
        String sql = "SELECT * FROM Topic WHERE id = " + id;
        return jdbcTemplate.query(sql,
                rs -> {
                    if (rs.next()) {
                        Topic topic = new Topic();

                        topic.setId(rs.getInt("id"));
                        topic.setName(rs.getString("title"));
                        topic.setDescription(rs.getString("description"));

                        return topic;
                    }

                    return null;
                });
    }

    @Override
    public void addTopic(Topic topic) {
        String sql = "INSERT INTO Topic (title, description) VALUES (?,?)";
        jdbcTemplate.update(sql, topic.getName(), topic.getDescription());
    }

    @Override
    public void updateTopic(int id, Topic topic) {

    }

    @Override
    public void deleteTopic(int id) {
        String sql = "DELETE FROM Topic WHERE id = ?";
        jdbcTemplate.update(sql, id);
    }
}
